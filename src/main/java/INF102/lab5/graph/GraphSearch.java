package INF102.lab5.graph;


import java.util.*;

/**
 * This class is used to conduct search algorithms of a graph
 */
public class GraphSearch<V> implements IGraphSearch<V> {

    private IGraph<V> graph;

    public GraphSearch(IGraph<V> graph) {
        this.graph = graph;
    }


    @Override
    public boolean connected(V u, V v) {
        Map<V, Boolean> visited = new HashMap<>();
        for (V node : graph.getNodes()) {
            visited.put(node, false);
        }
        return depthFirstTraversal(u, v, visited);
    }


    //Reference: oppgavegjennomgang
    //the solution I came up with fialed the not Connected Test and did not use recursion
    //my solution was adapted from: https://medium.com/@avinash.sarguru/graph-traversals-in-java-d0d29d775ca5
    public Boolean depthFirstTraversal(V u, V v, Map<V, Boolean> visited) {

        visited.put(u, true);

        for (V next : graph.getNeighbourhood(u)) {
            if (next.equals(v)) {
                return true;
            }

            if (!visited.get(next)) {
                if (depthFirstTraversal(next, v, visited)) {
                    return true;
                }
            }
        }
        return false;
    }
}
